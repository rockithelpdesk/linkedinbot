module.exports =  async (accessToken, uri = "me") => {
    const https = require("https")
    return new Promise((resolve, reject)=>{
        const req = https.request({
            hostname: "api.linkedin.com",
            method: "GET",
            path: `/v2/${uri}`,
            headers: {
                "Connection": "Keep-Alive",
                "Authorization": `Bearer ${accessToken}`,
                "Accept": "application/json"
            }
        }, res => {
            const body = []
            res.setEncoding("utf-8")
            res.on("data", chunk => body.push(chunk))
            res.on("error", e => {
                reject(e)
            })
            res.on("end", ()=>{
                let response = {res, body: JSON.parse(body.join(""))}
                if(res.statusCode == 400){
                    reject(response)
                } else {
                    resolve(response)
                }
            })
        })
        req.on("error", e => {
            console.error(e)
            reject(e)
        })
        req.end()
    })
}