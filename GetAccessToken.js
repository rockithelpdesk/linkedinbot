
module.exports =  async (code) => {
    const https = require("https")
    const queryString = require("querystring")
    const postData = queryString.stringify({
        "grant_type": "authorization_code",
        "code": code,
        "redirect_uri": "http://localhost:3000/auth/linkedin",
        "client_id": process.env.CLIENT_ID,
        "client_secret": process.env.CLIENT_SECRET
    })
    return new Promise((resolve, reject)=>{
        const req = https.request({
            hostname: "www.linkedin.com",
            method: "POST",
            path: "/oauth/v2/accessToken",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Content-Length": Buffer.byteLength(postData)
            }
        }, res => {
            const body = []
            res.setEncoding("utf-8")
            res.on("data", chunk => body.push(chunk))
            res.on("end", ()=>{
                if(res.statusCode == 400) reject({res, body})
                resolve({res, body})
            })
        })
        req.on("error", e => reject(e, null))
        req.write(postData)
        req.end()
    })
}