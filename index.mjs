import fs from "fs"

const handlers = {
    async change(fileName){
        if(fileName == "index.mjs") return
        console.log(fileName)
        try{
            let module = await import(`./${fileName}?${new Date()}`)
            console.log(module)
        }catch(e){
            console.log(e)
        }
    },
    async rename(fileName){
        console.log(`rename ${fileName}`)
        try{
            let module = await import(`./${fileName}?${new Date()}`)
            console.log(module)
        }catch(e){
            console.log(e)
        }
    }
}
const fileWasModified = async (event, fileName) => {
    if(handlers[event]) await handlers[event](fileName)
}

;(async args => {
    [0, 1].forEach(i=>args.shift())
    const files = args.slice()
    const watcher = await fs.watch("./", {encoding: "utf-8"}, fileWasModified)
    console.log(watcher)
})(process.argv)
