
module.exports = ()=>{
    class DisplayImage {
        constructor(urn){
            this.urn = urn
        }
    }
    class LocalizedName {
        constructor(name){
            this.en_US = name.en_US
            this.preferredLocale = name.preferredLocale
        }
    }
    class LocalizedCountry {
        constructor(country){
            this.country = country.country
            this.language = country.language
        }
    }
    class MeResponse {
        constructor(body = {
            localizedLastName: "",
            provilePicture: new DisplayImage(""),
            firstName: new LocalizedName({
                en_US: "",
                preferredLocale: new LocalizedCountry({
                    country: "",
                    language: "en"
                })
            }),
            lastName: new LocalizedName({
                en_US: "",
                preferredLocale: new LocalizedCountry({
                    country: "",
                    language: "en"
                })
            }),
            id: "",
            localizedFirstName: ""
        }){
            Object.keys(body).map(key=>this[key] = body[key])
        }
    }
    return {
        MeResponse,
        DisplayImage,
        LocalizedCountry,
        LocalizedName
    }
}