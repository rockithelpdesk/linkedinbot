module.exports =  async (form, accessToken) => {
    const https = require("https")
    const postData = JSON.stringify(form)
    return new Promise((resolve, reject)=>{
        const req = https.request({
            hostname: "api.linkedin.com",
            method: "POST",
            path: "/v2/shares",
            headers: {
                "Content-Type": "application/json",
                "Content-Length": Buffer.byteLength(postData),
                "Authorization": `Bearer ${accessToken}`,
                "Accept": "application/json"
            }
        }, res => {
            const body = []
            res.setEncoding("utf-8")
            res.on("data", chunk => body.push(chunk))
            res.on("end", ()=>{
                const output = JSON.parse(body.join(""))
                if(res.statusCode == 400) reject({res, output})
                resolve({res, output})
            })
        })
        req.on("error", e => reject(e))
        req.write(postData)
        req.end()
    })
}